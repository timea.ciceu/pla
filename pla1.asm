data SEGMENT
num1 DB 01h  ; First number
num2 DB 11h  ; Second number
result DB 00h ; Result storage
data ENDS

code SEGMENT
ASSUME CS:code, DS:data

start:
MOV AX, data
MOV DS, AX

MOV AL, [num1]  ; Load the first number into AL
SUB AL, [num2]  ; Subtract the second number from AL
MOV [result], AL  ; Store the result in result variable

MOV AH, 4CH
int 21H

code ENDS
END start
