data SEGMENT
    ta DB 02h, 05h, 03h
    tb DB 01h, 01h, 05h
data ENDS

code SEGMENT
    ASSUME CS:code, DS:data

addition_movsb PROC
    CLD
    LEA SI, ta
    LEA DI, tb

    REP MOVSB
    MOV AL, [SI]
    ADD AL, [DI]
    MOV [DI], AL

    RET 

disp PROC
    MOV CX, 3
    display_loop:

    MOV DL, [DI]  
    ADD DL, 30h 
    MOV AH, 02h
    INT 21h

    INC DI  
    LOOP display_loop
    RET
disp ENDP

start:
    MOV AX, data
    MOV DS, AX

    LEA SI, ta
    LEA DI, tb

    ; tb = tb + ta
    MOV CL, 2
    CALL addition_movsb
    MOV CL, 1
    CALL addition_movsb
    MOV CL, 0
    CALL addition_movsb

    CALL disp

    ; return
    MOV AH, 4Ch
    INT 21h

code ENDS
END start
