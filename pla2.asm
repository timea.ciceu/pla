data SEGMENT
    num1 DB 01h  ; First number
    num2 DB 11h  ; Second number
    add_result DB 00h ; Variable to store the addition result
    sub_result DB 00h ; Variable to store the subtraction result
data ENDS

code SEGMENT
    ASSUME CS:code, DS:data

start:
    MOV AX, data
    MOV DS, AX

    ; Calculate and store the addition result
    MOV AL, [num1]  ; Load the first number into AL
    ADD AL, [num2]  ; Add the second number to AL
    MOV [add_result], AL  ; Store the addition result

    ; Calculate and store the subtraction result
    MOV AL, [num1]  ; Load the first number into AL
    SUB AL, [num2]  ; Subtract the second number from AL
    MOV [sub_result], AL  ; Store the subtraction result

    ; Exit program
    MOV AH, 4CH
    INT 21H

code ENDS
END start
