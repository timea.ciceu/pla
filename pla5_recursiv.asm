.data sum DW 0 number DW 10                             ;pastram valoarea in sum

.code mov cx, number mov ax, number                     ;initializam acumulatorul cu 'number'

sum_recursive: sub ax, 1 add sum, ax loop sum_recursive ;adaugam valoarea acumulatorului in sum

mov dx, offset sum mov ah, 09h int 21h                  ;printam suma

mov ah, 4Ch int 21h                                     ;iesim din program

end
