section .data
    new_line db 0Ah, 0Dh, "$"

section .text
    global _start

    _start:

    ; Iterative Sum
    mov cx, 06h
    call sum_iterative
    call print

    ; Recursive Sum
    mov ax, 0
    mov bx, 06h
    mov dx, 0
    call sum_recursive
    call print

    ; Exit program
    mov ah, 4Ch
    int 21h

sum_iterative:
    ; Iterative sum procedure
    mov ax, cx
    inc ax
    mul cx
    sar ax, 1

    mov bx, ax
    ret

sum_recursive:
    ; Recursive sum procedure
    cmp bx, 0
    je go_back
    push bx
    dec bx
    call sum_recursive
    pop bx
    inc dx
    add ax, dx
go_back:
    mov bx, ax
    ret

print:
    ; Print procedure in hex
    mov al, bl
    mov ah, 00h
    mov cx, 2

convert_and_print:
    rol al, 4
    push ax
    and al, 0Fh
    add al, '0'
    cmp al, '9'
    jbe numeric_digit
    add al, 7
numeric_digit:
    mov dl, al
    mov ah, 02h
    int 21h
    pop ax
    loop convert_and_print

    ; Print new line
    mov dl, 0Ah
    mov ah, 02h
    int 21h

    ret
