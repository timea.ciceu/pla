.data sum DW 0                              ; variabila in care retinem suma;

.code mov cx, number                        ; setam loop counter-ul la valoarea 'number' mov ax, 0
 
loop_start: add ax, cx                      ; adaugam valoarea loop counter-ului in accumulator-ul loop_start

mov sum, ax                                 ; retinem resultatul in 'sum'

mov dx, offset sum mov ah, 09h int 21h      ; printam suma

mov ah, 4Ch int 21h                         ; iesim din program

end
