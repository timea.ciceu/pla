data SEGMENT
    num1 DW 01.0  ; primul nr
    num2 DW 11.0  ; al doilea
    res DW 00.0.  ; rezultatul
data ENDS

code SEGMENT
    ASSUME CS:code, DS:data

start:
    MOV AX, data
    MOV DS, AX

    FLD num1         ; bagam primul nr in FPU stack
    FSUB num2        ; scadem cel de-al doilea nr
    FSTP res         ; pastram rezultatul in memorie

                     ; convertim rezultatul in int
    FISTP res        ; pastram int-ul in memorie

    MOV AH, 4CH
    INT 21H

code ENDS
END start
